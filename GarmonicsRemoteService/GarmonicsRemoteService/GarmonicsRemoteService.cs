﻿using System;
using Accord.Math.Integration;
using Accord.Math;

namespace GarmonicsRemoteService
{    
    public class GarmonicsRemoteService : MarshalByRefObject, IGarmonicsRemoteService
    {
        public double d_m(int m, int d)
        {
            double answer = 1;
            if (d == 2)
            {
                answer = Math.Pow(2, m);
                for (int i = m + 1; i <= 2 * m; i++)
                {
                    answer /= i;
                }
                return answer;
            }
            for (int i = 1; i <= m; i++)
            {
                answer *= (d - 2 + i - 1) / (((d - 1) / 2.0 + i - 1) * i);
            }
            return answer /= Math.Pow(2, m);
        }
        public double C_m(int m, int d, double alpha)
        {
            double answer = Math.Pow(2, m);
            for (int i = 0; i < m; i++)
            {
                answer *= (d + alpha) / 2 + i;
            }
            return answer * d_m(m, d) * c(m, d, alpha);
        }
        public double c(int m, int d, double alpha)
        {
            return alpha * Math.Pow(2, alpha) * Gamma.Function((d + alpha) / 2)
                / (Math.Pow(Math.PI, 0.5) * Gamma.Function(1 - alpha / 2.0) * Gamma.Function((d - 1.0) / 2)
                * Gegenbauer(d, m, 1));
        }
        public double A(int d, double alpha)
        {
            return alpha * Math.Pow(2, alpha - 1) * Gamma.Function((d + alpha) / 2)
                / (Math.Pow(Math.PI, d / 2.0) * Gamma.Function(1 - alpha / 2));
        }
        public double Gegenbauer(int d, int m, double t)
        {
            double alpha = d / 2.0 - 1;
            if (alpha == 0 && m == 0) return 1;
            if (alpha == 0 && m == 1) return 2 * t;
            if (alpha == 0 && m == 2) return t * t;
            if (m == 0) return 1;
            if (m == 1) return 2 * alpha * t;
            if (t == 1)
            {
                double answer = 1;
                for (int i = 1; i <= m; i++)
                {
                    answer *= (d - 3.0 + i) / i;
                }
                return answer;
            }
            return (2 * t * (m + alpha - 1) * Gegenbauer(d, m - 1, t) - (m + 2 * alpha - 2) * Gegenbauer(d, m - 2, t)) / m;
        }
        public double GammaRoot(int d, int m, double alpha, double error, double L1, double L2)
        {
            double lambda = lambda_m(d, m, alpha);
            double a = L1;
            double b = L2;
            double midpoint = (b + a) / 2;
            bool possible = true;
            double tempa;
            double tempb;
            double tempmid;
            while (possible && (b - a > error))
            {
                midpoint = (b + a) / 2;
                tempa = K(d, a, alpha, m) + lambda;
                tempb = K(d, b, alpha, m) + lambda;
                tempmid = K(d, midpoint, alpha, m) + lambda;
                if (tempa == 0) return a;
                if (tempb == 0) return b;
                if (tempmid == 0) return tempmid;
                if ((tempa * tempmid) < 0)
                {
                    b = midpoint;
                }
                else if ((tempb * tempmid) < 0)
                {
                    a = midpoint;
                }
                else
                {
                    possible = false;
                }
            }
            if (!possible) return -1;
            return midpoint;
        }
        public double I_w_t_report(int d, double lambda, double alpha, int m, double t)
        {
            Func<double, double> u = (r) => Math.Pow(r, m - 1) * (1 - Math.Pow(r, lambda)) *
            (Math.Pow(r, alpha - lambda) - Math.Pow(r, d)) *
            Math.Pow(r * r - 2 * r * t + 1, -(d + alpha + 2 * m) / 2);
            return NonAdaptiveGaussKronrod.Integrate(u, 0, 1);
        }
        public double K(int d, double lambda, double alpha, int m)
        {
            Func<double, double> u = (t) => Math.Pow(1 - t * t, m + ((double)d - 3) / 2) *
            I_w_t_report(d, lambda, alpha, m, t);
            return (NonAdaptiveGaussKronrod.Integrate(u, -1, 0, 1e-5) +
                NonAdaptiveGaussKronrod.Integrate(u, 0, 1, 1e-5)) * C_m(m, d, alpha);
        }
        public double du_0(int d, double alpha, double t, double r)
        {            
            return (Math.Pow(r, d - 1) + Math.Pow(r, alpha - 1)) * Math.Pow(r * r - 2 * r * t + 1, -(d + alpha) / 2);
        }
        public double dlambda(int d, int m, double alpha, double t)
        {
            Func<double, double> f = (r) => du_0(d, alpha, t, r) * (Gegenbauer(d, m, t) - Gegenbauer(d, m, 1))
            * Math.Pow(1 - t * t, (d - 3.0) / 2);
            if (d == 2 && alpha == 1) return (Gegenbauer(d, m, t) - Gegenbauer(d, m, 1))
              * Math.Pow(1 - t * t, (d - 3.0) / 2)/(1-t);
            return NonAdaptiveGaussKronrod.Integrate(f, 0, 1, 1e-5);
        }
        public double lambda_m(int d, int m, double alpha)
        {
            Func<double, double> f = (t) => dlambda(d, m, alpha, t);
            return c(m, d, alpha) * (NonAdaptiveGaussKronrod.Integrate(f, -1, 0, 1e-5)
                + NonAdaptiveGaussKronrod.Integrate(f, 0, 1, 1e-5));
        }
    }
}

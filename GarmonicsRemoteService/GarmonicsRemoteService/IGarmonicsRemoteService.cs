﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GarmonicsRemoteService
{
    public interface IGarmonicsRemoteService
    {
        double Gegenbauer(int d, int m, double t);
        double c(int m, int d, double alpha);
        double C_m(int m, int d, double alpha);
        double d_m(int m, int d);
        double A(int d, double alpha);
        double GammaRoot(int d, int m, double alpha, double error, double L1, double L2);
        double I_w_t_report(int d, double lambda, double alpha, int m, double t);
        double K(int d, double lambda, double alpha, int m);
        double du_0(int d, double alpha, double t, double r);
        double dlambda(int d, int m, double alpha, double t);
        double lambda_m(int d, int m, double alpha);
    }
}

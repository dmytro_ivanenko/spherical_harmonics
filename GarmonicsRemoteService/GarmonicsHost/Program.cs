﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using GarmonicsRemoteService;

namespace GarmonicsHost
{
    class Program
    {
        static void Main(string[] args)
        {
            var channel = new TcpChannel(51495);
            ChannelServices.RegisterChannel(channel, false);
            var service = new GarmonicsRemoteService.GarmonicsRemoteService();
            RemotingConfiguration.RegisterWellKnownServiceType(typeof(GarmonicsRemoteService.GarmonicsRemoteService),
                "Garmonics", WellKnownObjectMode.SingleCall);            
            Console.WriteLine("Service started");
            Console.ReadLine();
        }
    }
}
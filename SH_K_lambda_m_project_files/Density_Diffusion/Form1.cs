﻿using System;
using System.Windows.Forms;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using GarmonicsRemoteService;

namespace Density_Diffusion
{   
    public partial class Form1 : Form
    {
        IGarmonicsRemoteService instance;
        public Form1()
        {
            InitializeComponent();
            var channel = new TcpChannel();
            ChannelServices.RegisterChannel(channel,false);            
            instance = (IGarmonicsRemoteService)Activator.GetObject(typeof(IGarmonicsRemoteService),
                "tcp://localhost:51495/Garmonics");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double alpha = double.Parse(Alpha.Text.Replace('.', ','));
            double error = double.Parse(Error.Text.Replace('.', ','));
            double ymin = double.Parse(L1.Text.Replace('.', ','));
            double ymax = double.Parse(L2.Text.Replace('.', ','));
            double T = double.Parse(time.Text.Replace('.', ','));
            int d = int.Parse(dimension.Text);
            int M = int.Parse(m.Text);
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            chart1.Series[2].Points.Clear();
            chart1.Series[3].Points.Clear();
            double Step = 0.1;
            int count = (int)Math.Ceiling((ymax - ymin) / Step) + 1;
            double[] yy = new double[count];
            yy[0] = ymin;
            double lambda = -instance.lambda_m(d, M, alpha);
            for (int i = 1; i < yy.Length; i++)
            {
                yy[i] = yy[i - 1] + Step;
                chart1.Series[0].Points.AddXY(yy[i], instance.K(d, yy[i], alpha, M));
                chart1.Series[1].Points.AddXY(yy[i], lambda);
                chart1.Series[2].Points.AddXY(yy[i], instance.I_w_t_report(d, yy[i], alpha, M, T));
            }
            double ans = instance.GammaRoot(d, M, alpha, error, ymin, ymax);
            string answer = Math.Round(ans, 6).ToString();
            chart1.Series[3].Points.AddXY(ans, lambda);
            gamma_answer.Text = answer;
            _lambda_m.Text = $"{Math.Round(lambda, 6)}";
            A_d_alpha.Text = $"{Math.Round(instance.A(d, alpha), 6)}";
            C_m.Text = $"{Math.Round(instance.c(M, d, alpha), 6)}";
            _C_m.Text = $"{Math.Round(instance.C_m(M, d, alpha), 6)}";
            chart1.ChartAreas[0].AxisX.MajorGrid.Interval = Step;
            chart1.ChartAreas[0].AxisX.MajorGrid.Interval = Step;
            chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
        }        
    }
}

﻿namespace Density_Diffusion
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.m = new System.Windows.Forms.TextBox();
            this.dimension = new System.Windows.Forms.TextBox();
            this.Alpha = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.A_d_alpha = new System.Windows.Forms.TextBox();
            this.C_m = new System.Windows.Forms.TextBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button1 = new System.Windows.Forms.Button();
            this.answer = new System.Windows.Forms.Label();
            this.gamma_answer = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Error = new System.Windows.Forms.TextBox();
            this.lambda_m = new System.Windows.Forms.Label();
            this._lambda_m = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.L1 = new System.Windows.Forms.TextBox();
            this.L2 = new System.Windows.Forms.TextBox();
            this.Const_m = new System.Windows.Forms.Label();
            this._C_m = new System.Windows.Forms.TextBox();
            this.T = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // m
            // 
            this.m.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.m.Location = new System.Drawing.Point(279, 54);
            this.m.Margin = new System.Windows.Forms.Padding(4);
            this.m.Name = "m";
            this.m.Size = new System.Drawing.Size(77, 30);
            this.m.TabIndex = 29;
            this.m.Text = "1";
            // 
            // dimension
            // 
            this.dimension.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dimension.Location = new System.Drawing.Point(279, 18);
            this.dimension.Margin = new System.Windows.Forms.Padding(4);
            this.dimension.Name = "dimension";
            this.dimension.Size = new System.Drawing.Size(77, 30);
            this.dimension.TabIndex = 28;
            this.dimension.Text = "3";
            // 
            // Alpha
            // 
            this.Alpha.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Alpha.Location = new System.Drawing.Point(453, 54);
            this.Alpha.Margin = new System.Windows.Forms.Padding(4);
            this.Alpha.Name = "Alpha";
            this.Alpha.Size = new System.Drawing.Size(100, 30);
            this.Alpha.TabIndex = 27;
            this.Alpha.Text = "1,5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(167, 57);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 25);
            this.label6.TabIndex = 21;
            this.label6.Text = "s.h.order m";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(167, 21);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 25);
            this.label5.TabIndex = 20;
            this.label5.Text = "Dimension";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(381, 59);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 25);
            this.label4.TabIndex = 19;
            this.label4.Text = "Alpha";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(1032, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 25);
            this.label2.TabIndex = 17;
            this.label2.Text = "c_m";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(1103, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 25);
            this.label1.TabIndex = 16;
            this.label1.Text = "A_d_alpha";
            // 
            // A_d_alpha
            // 
            this.A_d_alpha.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.A_d_alpha.Location = new System.Drawing.Point(1114, 54);
            this.A_d_alpha.Margin = new System.Windows.Forms.Padding(4);
            this.A_d_alpha.Name = "A_d_alpha";
            this.A_d_alpha.ReadOnly = true;
            this.A_d_alpha.Size = new System.Drawing.Size(96, 30);
            this.A_d_alpha.TabIndex = 34;
            this.A_d_alpha.TextChanged += new System.EventHandler(this.button1_Click);
            // 
            // C_m
            // 
            this.C_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.C_m.Location = new System.Drawing.Point(1022, 54);
            this.C_m.Margin = new System.Windows.Forms.Padding(4);
            this.C_m.Name = "C_m";
            this.C_m.ReadOnly = true;
            this.C_m.Size = new System.Drawing.Size(78, 30);
            this.C_m.TabIndex = 35;
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Enabled = false;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(39, 106);
            this.chart1.Margin = new System.Windows.Forms.Padding(4);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Legend = "Legend1";
            series1.Name = "K_lambda,m(d,alpha)";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Legend = "Legend1";
            series2.Name = "-lambda_m(d,alpha)";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Legend = "Legend1";
            series3.Name = "I_w(d,m,lambda,t)";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series4.EmptyPointStyle.AxisLabel = "lambda*";
            series4.Legend = "Legend1";
            series4.Name = "lambda*";
            series4.YValuesPerPoint = 2;
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Series.Add(series4);
            this.chart1.Size = new System.Drawing.Size(1273, 617);
            this.chart1.TabIndex = 36;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(39, 23);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 63);
            this.button1.TabIndex = 37;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // answer
            // 
            this.answer.AutoSize = true;
            this.answer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.answer.Location = new System.Drawing.Point(1212, 29);
            this.answer.Name = "answer";
            this.answer.Size = new System.Drawing.Size(84, 25);
            this.answer.TabIndex = 38;
            this.answer.Text = "lambda*";
            // 
            // gamma_answer
            // 
            this.gamma_answer.BackColor = System.Drawing.SystemColors.Info;
            this.gamma_answer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gamma_answer.Location = new System.Drawing.Point(1217, 54);
            this.gamma_answer.Name = "gamma_answer";
            this.gamma_answer.ReadOnly = true;
            this.gamma_answer.Size = new System.Drawing.Size(95, 30);
            this.gamma_answer.TabIndex = 39;
            this.gamma_answer.TextChanged += new System.EventHandler(this.button1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(390, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 25);
            this.label7.TabIndex = 40;
            this.label7.Text = "Error";
            // 
            // Error
            // 
            this.Error.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Error.Location = new System.Drawing.Point(453, 18);
            this.Error.Name = "Error";
            this.Error.Size = new System.Drawing.Size(100, 30);
            this.Error.TabIndex = 41;
            this.Error.Text = "0,0001";
            this.Error.TextChanged += new System.EventHandler(this.button1_Click);
            // 
            // lambda_m
            // 
            this.lambda_m.AutoSize = true;
            this.lambda_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lambda_m.Location = new System.Drawing.Point(822, 32);
            this.lambda_m.Name = "lambda_m";
            this.lambda_m.Size = new System.Drawing.Size(117, 25);
            this.lambda_m.TabIndex = 43;
            this.lambda_m.Text = "-Lambda_m";
            // 
            // _lambda_m
            // 
            this._lambda_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._lambda_m.Location = new System.Drawing.Point(819, 54);
            this._lambda_m.Name = "_lambda_m";
            this._lambda_m.ReadOnly = true;
            this._lambda_m.Size = new System.Drawing.Size(120, 30);
            this._lambda_m.TabIndex = 44;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(581, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 25);
            this.label3.TabIndex = 45;
            this.label3.Text = "lambda1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(581, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 25);
            this.label9.TabIndex = 46;
            this.label9.Text = "lambda2";
            // 
            // L1
            // 
            this.L1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.L1.Location = new System.Drawing.Point(674, 18);
            this.L1.Name = "L1";
            this.L1.Size = new System.Drawing.Size(56, 30);
            this.L1.TabIndex = 47;
            this.L1.Text = "0";
            // 
            // L2
            // 
            this.L2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.L2.Location = new System.Drawing.Point(674, 54);
            this.L2.Name = "L2";
            this.L2.Size = new System.Drawing.Size(56, 30);
            this.L2.TabIndex = 48;
            this.L2.Text = "2";
            // 
            // Const_m
            // 
            this.Const_m.AutoSize = true;
            this.Const_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Const_m.Location = new System.Drawing.Point(951, 32);
            this.Const_m.Name = "Const_m";
            this.Const_m.Size = new System.Drawing.Size(54, 25);
            this.Const_m.TabIndex = 49;
            this.Const_m.Text = "C_m";
            // 
            // _C_m
            // 
            this._C_m.BackColor = System.Drawing.SystemColors.Control;
            this._C_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._C_m.Location = new System.Drawing.Point(945, 54);
            this._C_m.Name = "_C_m";
            this._C_m.Size = new System.Drawing.Size(70, 30);
            this._C_m.TabIndex = 50;
            // 
            // T
            // 
            this.T.AutoSize = true;
            this.T.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.T.Location = new System.Drawing.Point(760, 21);
            this.T.Name = "T";
            this.T.Size = new System.Drawing.Size(17, 25);
            this.T.TabIndex = 51;
            this.T.Text = "t";
            // 
            // time
            // 
            this.time.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.time.Location = new System.Drawing.Point(753, 54);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(40, 30);
            this.time.TabIndex = 52;
            this.time.Text = "0.9";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1342, 736);
            this.Controls.Add(this.time);
            this.Controls.Add(this.T);
            this.Controls.Add(this._C_m);
            this.Controls.Add(this.Const_m);
            this.Controls.Add(this.L2);
            this.Controls.Add(this.L1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._lambda_m);
            this.Controls.Add(this.lambda_m);
            this.Controls.Add(this.Error);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.gamma_answer);
            this.Controls.Add(this.answer);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.A_d_alpha);
            this.Controls.Add(this.C_m);
            this.Controls.Add(this.m);
            this.Controls.Add(this.dimension);
            this.Controls.Add(this.Alpha);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Spherical Harmonics Application";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m;
        private System.Windows.Forms.TextBox dimension;
        private System.Windows.Forms.TextBox Alpha;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox A_d_alpha;
        private System.Windows.Forms.TextBox C_m;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label answer;
        private System.Windows.Forms.TextBox gamma_answer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Error;
        private System.Windows.Forms.Label lambda_m;
        private System.Windows.Forms.TextBox _lambda_m;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox L1;
        private System.Windows.Forms.TextBox L2;
        private System.Windows.Forms.Label Const_m;
        private System.Windows.Forms.TextBox _C_m;
        private System.Windows.Forms.Label T;
        private System.Windows.Forms.TextBox time;
    }
}

